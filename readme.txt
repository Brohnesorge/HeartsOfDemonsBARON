Thank you for playing Hearts of Demons - BARON! This readme will provide you with a quick rundown of the mod to allow you to get in there and play!
Make sure to check out the options to personalize your Baron experience! There are gameplay, item spawn and graphics options all availble!

First off, let's talk controls
You will need Main Fire, Alt Fire, Zoom and Reload to play the mod.

Now weapons...
Slot 1 is your claws. Main fire slashes, Alt fire ground slams. You can combo alt-fire after a claw to punch! You can hold down alt-fire after a groundslam to pickup a rock! You can claw barrels to pick them up and throw them!
You can find the Fury Rune to make your claws stronger and faster.
Slot 2 is your spells! You start with one spell: Flare (Baron Ball). Main Fire casts the spell, Reload cycles your spells forward, Zoom cycles them backwards. Alt-Fire does nothing...for now. You can find new spells in the world so keep an eye out for Tomes and Scrolls! 
There are 3 spells to find
Incineration, a fireball spell that you can charge up to make even stronger
Immolation, a close range flamethrower spell
Catastrophe, a widescale meteor attack. Very dangerous but very powerful.

The rest are guns! Main fire shoots, Alt Fire does a Magic Shot! Magic shots require you to find the Enchantment Rune first
Slot 3 is the Belial Flak Cannon. Has limited range but is super powerful up close. The magic shot is a shorter range cluster bomb shot!
Slot 4 is the M2HB and the GAU-19. These are both extremely powerful machine guns, and you can pick which one will spawn in the options! The Magic Shot sets enemies ablaze with magic fire!
Slot 5 is the Moloch Rocket Launcher. It rapidly shoots naplam missiles that set enemies and the ground on fire. The Magic Shot has the same magic fire as above.
Slot 6 is the Azazel Magic Autocannon. This powerful and accurate autocannon is a reliable weapon against all enemies. It has a special Magic Shot that can be used without the Enchantment Rune! It also becomes stronger and fancier as you find spells.

Now some of the mechanics.
You are big, slow and have momentum, but at full speed, you can trample smaller enemies.
You can find Soulgems. These raise your Baron Level (makes your spells cheaper to cast, stronger and benefits you in many other ways) and increase your Mana capacity
You can find Ammo Satchels. These are backpacks, plain and simple.
Scattered throughout are Soul Gem Fragements. Collect 5 of these to assemble a Soul Gem with all the benefits it provides.
Over the Beserk, you can find the Power Rune. This acts like a Berserk for your spells, raising them to max power for the level. It also makes them free to cast for around 45 seconds.

The rest you'll have to see for yourself! Have fun!

Comments, questions, concerns, bug reports or just wanna tell me how much I suck? Make sure to post in the ZDoom Forums thread, or join the Discord server!
