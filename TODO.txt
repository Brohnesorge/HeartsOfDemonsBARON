///Azazel Rework///
-Main Fire. Alt Fire. Zoom. Reload.
-Main fire...fires.
-Alt-Fire switches ammo in bullet mode and spell in MAGIC MODE.
-Reload activates auto-lock.
-Zoom activates MAGIC MODE. Requires Enchantment Rune.
-Bullets cannon shot AUTO rapid shot
-Flak single canister shot AUTO rabid shot
-Missiles cluster bomb AUTO micro missiles
-Baron Ball cluster cannon shot AUTO BB hitscans
-Fireball shoots explosion blast AUTO small explosion waves
-Immolation ripper fire streams AUTO ???

///Tome of Power///
-Fireball cluster explosions
-Immolation...uhh...

///Melee///
-RockThrow respects Enhanced/Carnage/Bruiser/BruiserCarnage
-Incorporate the fistings

//Immolation//
-Change alt-fire to D4 one: fiery small rock spires

//REBALANCES//
-Raise DR again
-Lower running speed
-Bull charge by double tapping sprint