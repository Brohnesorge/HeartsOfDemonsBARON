class HoDBanhammer : BaronBaseWeapon
{
  Default
  {
    Inventory.PickUpMessage "The Diablo Battle Hammer. A weapon to smite all your foes.";
    Weapon.SlotNumber 1;
	Weapon.SlotPriority 0;
    Tag "Diablo Battle Hammer";
    Weapon.BobRangeX 0.35;
    Weapon.BobSpeed 1.75;
    Weapon.BobStyle "Smooth";
    +WEAPON.NOALERT;
    +WEAPON.NOAUTOAIM;
    +WEAPON.NOAUTOFIRE;
    +WEAPON.MELEEWEAPON;
    +WEAPON.AMMO_OPTIONAL;
    +WEAPON.ALT_AMMO_OPTIONAL;
  }

  States
  {
    Spawn:
      TNT1 A 0 NoDelay A_JumpIfInventory("HoDBanhammer", 1, "SpawnAgain", AAPTR_PLAYER1);
      DIAP A 1;
      Loop;

    SpawnAgain:
      TNT1 A 0 A_SpawnItemEx("Chainsaw");
      Stop;

    Ready:
      DIAI A 1 A_WeaponReady();
      Loop;

	Select:
		DIAI A 1 A_Raise(12);
		DIAI A 0
		{
			A_SetRoll(0, SPF_INTERPOLATE);
			A_ZoomFactor(1.00);
			A_SetCrosshair(99);
		}
		Loop;
	Deselect:
		DIAI A 1 A_Lower(12);
		DIAI A 0
		{
			A_SetRoll(0, SPF_INTERPOLATE);
			A_ZoomFactor(1.00);
		}
		Loop;

    Fire:
      DIAI AAAAAA 1 A_WeaponOffset(24, 16, WOF_ADD);
		TNT1 AAAAA 1
		{
			A_SetAngle(Angle - 1.5);
			A_SetRoll(Roll - 0.5);
		}
      TNT1 A 10 
	  {
		A_WeaponReady(WRF_NOFIRE);
		player.cheats |= CF_TOTALLYFROZEN;
		if (CheckInventory("Fury", 1))
		{
			A_SetTics(4);
		}
	}
      DIAH A 1
		{
			A_SetRoll(Roll + 2.5);
			A_SetAngle(Angle + 5);
		}
      DIAH B 1 
		{
			A_FireProjectile("DiabloHitter", 0, FALSE, 18);
			A_SetAngle(Angle + 2.5);
		}
      DIAH C 1 
		{
			A_CustomPunch(CheckInventory("PowerStrength", 0) ? random(1600, 3200) : random(400, 800), true, 0, "DiabloPuff", 128, 0, 0, "BasicArmorBonus", 0, "ClawMiss");
			A_SetAngle(Angle + 2.5);
		}	
      DIAH D 1
      {
        A_WeaponOffset(-64, 0, WOF_ADD);
        A_FireProjectile("DiabloHitter", 0, FALSE, -18);
		A_SetAngle(Angle + 2.5);
      }
      DIAH DD 1 
		{
			A_WeaponOffset(-64, 0, WOF_ADD);
			A_SetAngle(Angle + 2.5);
			A_SetRoll(Roll + 0.5);
		}
      TNT1 A 15
	  {
		player.cheats &= ~CF_TOTALLYFROZEN;
		if (CheckInventory("Carnage", 1))
		{
			A_SetTics(7);
		}
		}
      DIAI A 0 A_WeaponOffset(20, 72);
      DIAI AAAAAAAAAA 1 
		{
			A_WeaponOffset(-2, -5, WOF_ADD);
			A_SetRoll(Roll - 0.1);
		}
      Goto Ready;

    AltFire:
      DIAI AAAAAA 1 A_WeaponOffset(24, 16, WOF_ADD);
		TNT1 AAAAA 1 A_SetPitch(pitch - 0.35);
		TNT1 A 10 
		{
			A_WeaponReady(WRF_NOFIRE);
			player.cheats |= CF_TOTALLYFROZEN;
			if (CheckInventory("Fury", 1))
			{
				A_SetTics(4);
			}
		}
      DIAS ABCD 1 A_SetPitch(Pitch + 1);
      DIAS E 1 A_CustomPunch(CheckInventory("PowerStrength", 0) ? 4800 : 1200, true, 0, "FalconPunchPuff", 128, 0, 0, "BasicArmorBonus", 0, "ClawMiss");
      TNT1 A 15
      {
        if(pitch >= 35 && CheckInventory("BaronMana", 5))
        {
          let plr = TheBaron(self);
          if (plr && plr.EnchantmentRune)
          {
            A_TakeInventory("BaronMana", 5);
			if (plr && plr.SpellBoostRune)
			{
				A_FireProjectile("DiabloBolt");
			}
			else A_FireProjectile("DiabloWave");
          }
        }
		player.cheats &= ~CF_TOTALLYFROZEN;
		if (CheckInventory("Carnage", 1))
		{
			A_SetTics(7);
		}
      }
      DIAI A 0 A_WeaponOffset(20, 72);
      DIAI AAAAAAAAAA 1 A_WeaponOffset(-2, -5, WOF_ADD);
      Goto Ready;
  }
}

class DiabloHitter : FastProjectile
{
  Default
  {
    Height          4;
    Radius          8;
    Speed           128;

    DamageFunction  random(200, 400);

    Renderstyle    "Add";
    Decal          "WallCracks";
    DamageType     "WTFBoom";

    +BloodSplatter;
    +NODAMAGETHRUST;
    +ForcePain;
  }

  States
  {
    Spawn:
      TNT1 A 1 NoDelay;
      Stop;

    XDeath:
      YTHO A 2 Bright
      {
        if(hod_quake)
        {
          A_QuakeEx(0, 2, 2, 95, 0, 128, "Null", QF_SCALEDOWN|QF_RELATIVE);
        }
        A_PlaySound("DIAHIT1",CHAN_7);
        A_PlaySound("FistGore", CHAN_6);
      }
      YTHO BCDEF 2 Bright
      {
        A_SetScale(Scale.X + 0.1);
        A_FadeTo(0, 0.2, FALSE);
      }
      Stop;

    Crash:
      TNT1 A 0
      {

        if (hod_rubble)
        {
          for (int i = 0; i < 25; ++i)
            A_SpawnItemEx("RubbleSpawner", random(-5, 5), random(-5, 5),0,frandom(-10,0), frandom(-12,12),frandom(-12,12));
        }
        if (hod_smoke)
        {
          for (int i = 0; i < 2; ++i)
            A_SpawnProjectile("DebrisSmoke",0,0,frandom(-15, 15),CMF_AIMDIRECTION, frandom(-15,15));
        }

      }
      YTHO A 2 Bright
      {
        if(hod_quake)
        {
          A_QuakeEx(0, 2, 2, 95, 0, 128, "Null", QF_SCALEDOWN|QF_RELATIVE);
        }
        A_PlaySound("Weapons/Punch",CHAN_7);

      }
      YTHO BCDEF 1 Bright
      {
        A_SetScale(Scale.X + 0.1);
        A_FadeTo(0, 0.2, FALSE);
      }
      Stop;

  }
}

class DiabloWave : Actor
{
  Default
  {
    Projectile;

    Height         4;
    Radius         4;
    Speed          20;
    DamageFunction 500;

    Scale          1.0;
    Alpha          0.65;

    Renderstyle    "Add";
    DamageType     "Magic";

    +FLOORHUGGER
    +STEPMISSILE;
    +SEEKERMISSILE;
    +SCREENSEEKER;
    +NODAMAGETHRUST;
    +BRIGHT;
  }

  States
  {
    Spawn:
      TNT1 AAAAAAAAA 5 NoDelay
      {
        A_SeekerMissile(33, 45, SMF_LOOK);
        A_SpawnItemEX("GreenFireAfterImage");
      }
      Stop;

    Death:
      TNT1 A 0
      {
        A_Explode(320, 64, 0, 64);
        A_StartSound("Fwosh1", 19);
      }
      IMWV ABCDEFGHIJKLMNOPQRST 1;
      Stop;
  }
}

class DiabloBolt : DiabloWave 
{
	Default
	{
		Speed 35;
	}
	
	States 
	{
		 Spawn:
		  TNT1 AAAAAAAAA 5 NoDelay
		  {
			A_SeekerMissile(33, 45, SMF_LOOK);
			A_SpawnItemEX("DiabloBoltTrail");
		  }
		  Stop;

		Death:
		  TNT1 A 0
		  {
			A_SpawnItemEx("MAXLIGHTNINGBOLT");
		  }
		  BMWV ABCDEFGHIJKLMNOPQRST 1;
		  Stop;
	}
}

class DiabloBoltTrail : Actor 
{
	Default
	{
		Alpha 1.0;
		XScale 0.15;
		YScale 1.0;
		+ROLLSPRITE;
		+ROLLCENTER;
		+BRIGHT;
	}

	States
	{
		Spawn:
			RBOL G 1 NoDelay
			{
				if (!random(0, 1))
				{
					invoker.bSPRITEFLIP = TRUE;
				}
			}
			RBOL GG 1 A_FadeTo(0, 0.5, TRUE);
			Stop;
	}
}