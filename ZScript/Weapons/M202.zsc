class M202 : BaronBaseWeapon replaces RocketLauncher
{
	action void A_FireWeapon()
	{
		A_PlaySound("weapons/RocketLaunch",CHAN_WEAPON);
		A_PlaySound("GRENFIRE", CHAN_VOICE);
		A_AlertMonsters();
		if(hod_smoke)
		{
			for (int i = 0; i < 3; ++i)
				A_FireProjectile("RocketSmoke", frandom(-25, 25), FALSE, 8 , 1, FPF_NOAUTOAIM);
		}

		if(hod_pushback)
		{
			A_Recoil((CheckInventory("Carnage", 0) ? 0.02 : 0.25) * cos(pitch));
		}
		if (hod_recoil)
		{
			A_SetPitch(Pitch - (CheckInventory("Carnage", 0) ? 0.5 : 1.85), SPF_INTERPOLATE);
		}
		if(hod_quake)
		{
			A_QuakeEx2(2, 0, 0, 10, 0, 10);
		}
	}

	action void A_NewFlash()
	{
		A_Overlay(-6, "MuzzleFlash");
		A_Overlay(6, "Highlights");
	}

	action void A_MagicFlash()
	{
		A_Overlay(-6, "GMuzzleFlash");
		A_Overlay(6, "GHighlights");
		A_Overlay(-7, "GlowOut");
		A_Overlay(-5, "Circle");
	}

	int BarrelReady;
	int MissilesLoaded;

	Default
	{
		Inventory.PickupMessage "The Moloch Multi Rocket Cannon. Blow your enemies apart. Burn them to ashes.";
		Tag "Moloch Rocket Multi Cannon";
		Weapon.AmmoType "MMissile";
		Weapon.AmmoType2 "MMissile";
		Weapon.AmmoUse 1;
		Weapon.AmmoUse2 1;
		Weapon.AmmoGive 5;
		Weapon.SlotNumber 5;
		Weapon.BobRangeX 0.25;
		Weapon.BobRangeY 0.5;
		Weapon.BobSpeed 1;
		Scale 0.65;
		+WEAPON.NOAUTOAIM
		+WEAPON.NOALERT
	}

	States
	{

		// ----------------------- //
		// ----- ITEM STATES ----- //
		// ----------------------- //

		Spawn:
			TNT1 A 1 NoDelay
			{
				if(random(1, 2) == 2)
				{
					Return ResolveState("TomeDrop");
				}

				Return ResolveState(null);

			}
			M20I A -1
			{
				
				if (invoker.bDropped || !hod_moloch_spawn)
				{
					Return ResolveState("AmmoDrop");
				}

				int LawcherToSpawn = CVar.FindCvar("hod_lawnchair").GetInt();

				if (LawcherToSpawn == 1 || random(1, 2) == 2 && LawcherToSpawn == 2)
				{
					Return ResolveState("SpawnMKInstead");
				}

				Return ResolveState(null);
			}
			Stop;

		AmmoDrop:
			TNT1 A 0 A_SpawnItemEx("MMissile");
			Stop;

		SpawnMKInstead:
			TNT1 A 0 A_SpawnItemEx("HOD_MK19");
			Stop;

		TomeDrop:
			TNT1 A 0 A_SpawnItemEX("Tier2SpellDrop");
			Stop;

		// ------------------------- //
		// ----- WEAPON STATES ----- //
		// ------------------------- //

		Ready:
			TNT1 A 0
			{
				let plr = TheBaron(self);
				if (plr && plr.BruiserMode & hod_melt)
				{
					Return ResolveState("BurnedAway");
				}
				Return ResolveState(null);
			}
			M202 A 1 A_WeaponReady();
			Loop;

		BurnedAway:
			M202 A 1
			{
				A_SelectWeapon("BruiserHands");
				A_TakeInventory("M202");
				A_Print("The weapon melts in your hands...");
			}
			Goto Ready;

		// ----------------------- //
		// ----- OUT OF MANA ----- //
		// ----------------------- //

		OOM:
			TNT1 A 0
			{
				A_Print("Insufficent Mana...");
				A_PlaySound("CASTFAIL", CHAN_WEAPON);
			}
			Goto Ready;

		// ------------------------------- //
		// ----- SELECT AND DESELECT ----- //
		// ------------------------------- //

		Select:
			TNT1 A 0 A_SetCrosshair(32);
			M202 A 1 A_Raise(12);
			Loop;

		Deselect:
			M202 A 1 A_Lower(12);
			Loop;

		// ---------------------------------- //
		// ----- MAGIC SHOT OVERLAY VFX ----- //
		// ---------------------------------- //

		GlowOut:
			TNT1 A 0 A_OverlayFlags(-7, PSPF_ALPHA|PSPF_FORCEALPHA, TRUE);
			TNT1 A 0 A_OverlayAlpha(-7, 0);
			MGLO A 1 Bright A_OverlayAlpha(-7, 1.0);
			MGLO A 1 Bright A_OverlayAlpha(-7, 0.8);
			MGLO A 1 Bright A_OverlayAlpha(-7, 0.6);
			MGLO A 1 Bright A_OverlayAlpha(-7, 0.4);
			MGLO A 1 Bright A_OverlayAlpha(-7, 0.2);
			Stop;

		Circle:
			TNT1 A 0 A_OverlayFlags(-5, PSPF_ALPHA|PSPF_FORCEALPHA|PSPF_RENDERSTYLE, TRUE);
			TNT1 A 0 A_OverlayFlags(-5, PSPF_ADDWEAPON|PSPF_ADDBOB, FALSE);
			TNT1 A 0 A_OverlayRenderstyle(-5, STYLE_ADD);
			TNT1 A 0 A_OverlayOffset(-5, 6, 15);
			M2CR A 3 Bright A_OverlayAlpha(-5, 1);
			M2CR A 1 Bright A_OverlayAlpha(-5, 0.8);
			M2CR A 1 Bright A_OverlayAlpha(-5, 0.6);
			M2CR A 1 Bright A_OverlayAlpha(-5, 0.4);
			M2CR A 1 Bright A_OverlayAlpha(-5, 0.2);
			M2CR A 1 Bright A_OverlayAlpha(-5, 0.0);
			Stop;

		// -------------------------------- //
		// ----- MUZZLEFLASH OVERLAYS ----- //
		// -------------------------------- //

		MuzzleFlash:
			TNT1 A 0 A_JumpIf(invoker.BarrelReady == 1, "MuzzleFlash2");
			TNT1 A 0 A_JumpIf(invoker.BarrelReady == 2, "MuzzleFlash3");
			TNT1 A 0 A_JumpIf(invoker.BarrelReady == 3, "MuzzleFlash4");
			TNT1 A 0 A_JumpIf(invoker.BarrelReady == 4, "MuzzleFlash5");
			TNT1 A 0 A_JumpIf(invoker.BarrelReady == 5, "MuzzleFlash6");
			TNT1 A 0 A_JumpIf(invoker.Enchanted == TRUE, "GMuzzleFlash");
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ALPHA|PSPF_FORCEALPHA, TRUE);
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ADDWEAPON|PSPF_ADDBOB, FALSE);
			MOFL A 1 Bright
			{
				invoker.BarrelReady ++;
				A_FireProjectile("MolochMiss", 0, TRUE, 8 , -4, FPF_NOAUTOAIM);
			}
			MOFL A 1 Bright A_OverlayAlpha(-6, 0.8);
			MOFL A 1 Bright A_OverlayAlpha(-6, 0.6);
			MOFL A 1 Bright A_OverlayAlpha(-6, 0.4);
			MOFL A 1 Bright A_OverlayAlpha(-6, 0.2);
			MOFL A 1 Bright A_OverlayAlpha(-6, 0.0);
			Stop;

		MuzzleFlash2:
			TNT1 A 0 A_JumpIf(invoker.Enchanted == TRUE, "GMuzzleFlash2");
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ALPHA|PSPF_FORCEALPHA, TRUE);
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ADDWEAPON|PSPF_ADDBOB, FALSE);
			MOFL B 1 Bright
			{
				invoker.BarrelReady ++;
				A_FireProjectile("MolochMiss", 0, TRUE, 16 , -4, FPF_NOAUTOAIM);
			}
			MOFL B 1 Bright A_OverlayAlpha(-6, 0.8);
			MOFL B 1 Bright A_OverlayAlpha(-6, 0.6);
			MOFL B 1 Bright A_OverlayAlpha(-6, 0.4);
			MOFL B 1 Bright A_OverlayAlpha(-6, 0.2);
			MOFL B 1 Bright A_OverlayAlpha(-6, 0.0);
			Stop;

		MuzzleFlash3:
			TNT1 A 0 A_JumpIf(invoker.Enchanted == TRUE, "GMuzzleFlash3");
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ALPHA|PSPF_FORCEALPHA, TRUE);
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ADDWEAPON|PSPF_ADDBOB, FALSE);
			MOFL C 1 Bright
			{
				invoker.BarrelReady ++;
				A_FireProjectile("MolochMiss", 0, TRUE, 24 , -4, FPF_NOAUTOAIM);
			}
			MOFL C 1 Bright A_OverlayAlpha(-6, 0.8);
			MOFL C 1 Bright A_OverlayAlpha(-6, 0.6);
			MOFL C 1 Bright A_OverlayAlpha(-6, 0.4);
			MOFL C 1 Bright A_OverlayAlpha(-6, 0.2);
			MOFL C 1 Bright A_OverlayAlpha(-6, 0.0);
			Stop;

		MuzzleFlash4:
			TNT1 A 0 A_JumpIf(invoker.Enchanted == TRUE, "GMuzzleFlash4");
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ALPHA|PSPF_FORCEALPHA, TRUE);
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ADDWEAPON|PSPF_ADDBOB, FALSE);
			MOFL D 1 Bright
			{
				invoker.BarrelReady ++;
				A_FireProjectile("MolochMiss", 0, TRUE, 8 , -12, FPF_NOAUTOAIM);
			}
			MOFL D 1 Bright A_OverlayAlpha(-6, 0.8);
			MOFL D 1 Bright A_OverlayAlpha(-6, 0.6);
			MOFL D 1 Bright A_OverlayAlpha(-6, 0.4);
			MOFL D 1 Bright A_OverlayAlpha(-6, 0.2);
			MOFL D 1 Bright A_OverlayAlpha(-6, 0.0);
			Stop;

		MuzzleFlash5:
			TNT1 A 0 A_JumpIf(invoker.Enchanted == TRUE, "GMuzzleFlash5");
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ALPHA|PSPF_FORCEALPHA, TRUE);
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ADDWEAPON|PSPF_ADDBOB, FALSE);
			MOFL E 1 Bright
			{
				invoker.BarrelReady ++;
				A_FireProjectile("MolochMiss", 0, TRUE, 16, -12, FPF_NOAUTOAIM);
			}
			MOFL E 1 Bright A_OverlayAlpha(-6, 0.8);
			MOFL E 1 Bright A_OverlayAlpha(-6, 0.6);
			MOFL E 1 Bright A_OverlayAlpha(-6, 0.4);
			MOFL E 1 Bright A_OverlayAlpha(-6, 0.2);
			MOFL E 1 Bright A_OverlayAlpha(-6, 0.0);
			Stop;

		MuzzleFlash6:
			TNT1 A 0 A_JumpIf(invoker.Enchanted == TRUE, "GMuzzleFlash6");
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ALPHA|PSPF_FORCEALPHA, TRUE);
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ADDWEAPON|PSPF_ADDBOB, FALSE);
			MOFL F 1 Bright
			{
				invoker.BarrelReady = 0;
				A_FireProjectile("MolochMiss", 0, TRUE, 24 , -12, FPF_NOAUTOAIM);
			}
			MOFL F 1 Bright A_OverlayAlpha(-6, 0.8);
			MOFL F 1 Bright A_OverlayAlpha(-6, 0.6);
			MOFL F 1 Bright A_OverlayAlpha(-6, 0.4);
			MOFL F 1 Bright A_OverlayAlpha(-6, 0.2);
			MOFL F 1 Bright A_OverlayAlpha(-6, 0.0);
			Stop;

		GMuzzleFlash:
			TNT1 A 0 A_JumpIf(invoker.BarrelReady == 1, "GMuzzleFlash2");
			TNT1 A 0 A_JumpIf(invoker.BarrelReady == 2, "GMuzzleFlash3");
			TNT1 A 0 A_JumpIf(invoker.BarrelReady == 3, "GMuzzleFlash4");
			TNT1 A 0 A_JumpIf(invoker.BarrelReady == 4, "GMuzzleFlash5");
			TNT1 A 0 A_JumpIf(invoker.BarrelReady == 5, "GMuzzleFlash6");

			TNT1 A 0 A_OverlayFlags(-6, PSPF_ALPHA|PSPF_FORCEALPHA, TRUE);
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ADDWEAPON|PSPF_ADDBOB, FALSE);
			GOFL A 1 Bright
			{
				invoker.BarrelReady ++;
				A_FireProjectile("MagicMolochMiss", 0, TRUE, 8 , -4, FPF_NOAUTOAIM);
			}
			GOFL A 1 Bright A_OverlayAlpha(-6, 0.8);
			GOFL A 1 Bright A_OverlayAlpha(-6, 0.6);
			GOFL A 1 Bright A_OverlayAlpha(-6, 0.4);
			GOFL A 1 Bright A_OverlayAlpha(-6, 0.2);
			GOFL A 1 Bright A_OverlayAlpha(-6, 0.0);
			Stop;

		GMuzzleFlash2:
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ALPHA|PSPF_FORCEALPHA, TRUE);
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ADDWEAPON|PSPF_ADDBOB, FALSE);
			GOFL B 1 Bright
			{
				invoker.BarrelReady ++;
				A_FireProjectile("MagicMolochMiss", 0, TRUE, 16 , -4, FPF_NOAUTOAIM);
			}
			GOFL B 1 Bright A_OverlayAlpha(-6, 0.8);
			GOFL B 1 Bright A_OverlayAlpha(-6, 0.6);
			GOFL B 1 Bright A_OverlayAlpha(-6, 0.4);
			GOFL B 1 Bright A_OverlayAlpha(-6, 0.2);
			GOFL B 1 Bright A_OverlayAlpha(-6, 0.0);
			Stop;

		GMuzzleFlash3:
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ALPHA|PSPF_FORCEALPHA, TRUE);
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ADDWEAPON|PSPF_ADDBOB, FALSE);
			GOFL C 1 Bright
			{
				invoker.BarrelReady ++;
				A_FireProjectile("MagicMolochMiss", 0, TRUE, 24 , -4, FPF_NOAUTOAIM);
			}
			GOFL C 1 Bright A_OverlayAlpha(-6, 0.8);
			GOFL C 1 Bright A_OverlayAlpha(-6, 0.6);
			GOFL C 1 Bright A_OverlayAlpha(-6, 0.4);
			GOFL C 1 Bright A_OverlayAlpha(-6, 0.2);
			GOFL C 1 Bright A_OverlayAlpha(-6, 0.0);
			Stop;

		GMuzzleFlash4:
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ALPHA|PSPF_FORCEALPHA, TRUE);
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ADDWEAPON|PSPF_ADDBOB, FALSE);
			GOFL D 1 Bright
			{
				invoker.BarrelReady ++;
				A_FireProjectile("MagicMolochMiss", 0, TRUE, 8 , -12, FPF_NOAUTOAIM);
			}
			GOFL D 1 Bright A_OverlayAlpha(-6, 0.8);
			GOFL D 1 Bright A_OverlayAlpha(-6, 0.6);
			GOFL D 1 Bright A_OverlayAlpha(-6, 0.4);
			GOFL D 1 Bright A_OverlayAlpha(-6, 0.2);
			GOFL D 1 Bright A_OverlayAlpha(-6, 0.0);
			Stop;

		GMuzzleFlash5:
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ALPHA|PSPF_FORCEALPHA, TRUE);
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ADDWEAPON|PSPF_ADDBOB, FALSE);
			GOFL E 1 Bright
			{
				invoker.BarrelReady ++;
				A_FireProjectile("MagicMolochMiss", 0, TRUE, 16, -12, FPF_NOAUTOAIM);
			}
			GOFL E 1 Bright A_OverlayAlpha(-6, 0.8);
			GOFL E 1 Bright A_OverlayAlpha(-6, 0.6);
			GOFL E 1 Bright A_OverlayAlpha(-6, 0.4);
			GOFL E 1 Bright A_OverlayAlpha(-6, 0.2);
			GOFL E 1 Bright A_OverlayAlpha(-6, 0.0);
			Stop;

		GMuzzleFlash6:
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ALPHA|PSPF_FORCEALPHA, TRUE);
			TNT1 A 0 A_OverlayFlags(-6, PSPF_ADDWEAPON|PSPF_ADDBOB, FALSE);
			GOFL F 1 Bright
			{
				invoker.BarrelReady = 0;
				A_FireProjectile("MagicMolochMiss", 0, TRUE, 24 , -12, FPF_NOAUTOAIM);
			}
			GOFL F 1 Bright A_OverlayAlpha(-6, 0.8);
			GOFL F 1 Bright A_OverlayAlpha(-6, 0.6);
			GOFL F 1 Bright A_OverlayAlpha(-6, 0.4);
			GOFL F 1 Bright A_OverlayAlpha(-6, 0.2);
			GOFL F 1 Bright A_OverlayAlpha(-6, 0.0);
			Stop;

		Highlights:
			TNT1 A 0 A_JumpIf(invoker.Enchanted == TRUE, "GHighLights");
			TNT1 A 0 A_OverlayFlags(6, PSPF_ALPHA|PSPF_FORCEALPHA|PSPF_ADDWEAPON, TRUE);
			MOHL A 1 Bright;
			MOHL A 1 Bright A_OverlayAlpha(6, 0.75);
			MOHL A 1 Bright A_OverlayAlpha(6, 0.50);
			MOHL A 1 Bright A_OverlayAlpha(6, 0.25);
			MOHL A 1 Bright A_OverlayAlpha(6, 0.00);
			Stop;

		GHighlights:
			TNT1 A 0 A_OverlayFlags(6, PSPF_ALPHA|PSPF_FORCEALPHA|PSPF_ADDWEAPON, TRUE);
			MOHL B 1 Bright;
			MOHL B 1 Bright A_OverlayAlpha(6, 0.75);
			MOHL B 1 Bright A_OverlayAlpha(6, 0.50);
			MOHL B 1 Bright A_OverlayAlpha(6, 0.25);
			MOHL B 1 Bright A_OverlayAlpha(6, 0.00);
			Stop;

		// ----------------------- //
		// ----- NORMAL SHOT ----- //
		// ----------------------- //


		Fire:
			M202 A 1
			{
				A_FireWeapon();
				A_NewFlash();
			}
			M202 A 1 A_WeaponOffset(9, 43);
			M202 A 3;
			M202 AAAAAA 1 A_WeaponOffset(-1.5, -1.83333,WOF_ADD);
			Goto Ready;

		// ---------------------- //
		// ----- MAGIC SHOT ----- //
		// ---------------------- //

		AltFire:
			TNT1 A 0
			{
				let plr = TheBaron(self);
				if (plr && !plr.EnchantmentRune)
				{
					A_Print("You lack the proper \cdRune...");
					return ResolveState("Ready");
				}
				Return ResolveState(null);
			}
			TNT1 A 0 A_TakeMana(30, FALSE, "OOM", 0);
			M202 A 1
			{
				A_FireWeapon();
				A_MagicFlash();
			}
			M202 A 1 A_WeaponOffset(6, 43);
			M202 A 3;
			M202 AAAAAA 1 A_WeaponOffset(-1.5, -1.83333,WOF_ADD);
			Goto Ready;

	}
}
