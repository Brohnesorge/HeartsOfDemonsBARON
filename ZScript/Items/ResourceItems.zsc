//------------------------------------------------------
//
// BASE CLASSES
//
//------------------------------------------------------

class BaronHealth : Health { Default { Inventory.MaxAmount 3000; } } //This one can overflow.
class BaronMana : Ammo replaces Clip
{
	Default
	{
		Inventory.Amount 10;
		Inventory.MaxAmount 100;
		Inventory.PickupMessage "You've consumed the soul. It restores your arcane energy and fortifies your body.";
	}
}

class ResourceItemBase : CustomInventory
{
	action void A_GiveSoulgem(int soulgems, int addlevel = 0)
	{
		let plr = TheBaron(self);
		if (plr)
		{
			//Increase level.
			if (addlevel > 0 && plr.BaronLevel < plr.MaxLevel)
			{
				plr.BaronLevel += addlevel;
				A_Log("\cdMagic Level Increased!");
			}

			//This upgrades the Fury rune to Carnage upon reaching max level.
			if (plr.BaronLevel == plr.MaxLevel && CheckInventory("Fury", 1))
			{
				A_GiveInventory("Carnage");
			}

			//Increase mana by 50 * soulgems.
			let mana = BaronMana(plr.FindInventory("BaronMana"));
			if (mana)
			{
				mana.MaxAmount += 50 * soulgems;
				A_Print("Mana Capacity Increased!");
			}

			//Increase soulgem counter.
			plr.Soulgems += soulgems;
		}
	}

	action void A_RestoreResource(int health, int overflowhealth, int mana)
	{
		let plr = TheBaron(self);
		if (plr)
		{
			if (health > 0) A_GiveInventory("Health", health * (1.0 + (10.0 / 100.0) * plr.BaronLevel));
			if (overflowhealth > 0) A_GiveInventory("BaronHealth", overflowhealth * (1.0 + (10.0 / 100.0) * plr.BaronLevel));
			if (mana > 0) A_GiveInventory("BaronMana", mana);
		}
	}
}

//------------------------------------------------------
//
// HEALTH ITEMS
//
//------------------------------------------------------

class NewBerserk : CustomInventory
{
	Default
	{
		Inventory.PickUpMessage "Rage fills your body, and your wounds are healed slightly. What was in this pack?";
		Inventory.PickUPsound "items/Berserksack";
	}

	States
	{
		Spawn:
			BERK A -1 NoDelay
			{
				if (!hod_zerk_spawn)
				{
					return ResolveState("DontSpawn");
				}
				Return ResolveState(null);
			}
			Stop;

		DontSpawn:
			TNT1 A 0;
			Stop;

		Pickup:
			TNT1 A 0
			{
				A_GiveInventory("PowerStrength");
				if (hod_zerk_extra)
				{
					A_GiveInventory("BerserkArmor");
					A_GiveInventory("BerserkRegen");
				}
				A_PlaySound("items/Berserk", 1, CHAN_VOICE);
			}
			//TNT1 A 0 HealThing(50, 0);
			Stop;
	}
}

class BerserkArmor : PowerProtection
{
	Default
	{
		Powerup.Duration -30;
		DamageFactor "Normal", 0.25;
	}
}

class BerserkRegen : PowerRegeneration
{
	Default
	{
		Powerup.Duration -30;
		Powerup.Strength 10;
	}
}


class NewStimpack: Stimpack replaces Stimpack
{
	Default
	{
		Inventory.Amount 1;
		Inventory.PickUpMessage "The needle barely breaks the skin. This stim is nearly useless.";
	}
}

class NewMedikit : Medikit replaces Medikit
{
	Default
	{
		Inventory.Amount 5;
		Inventory.PickupMessage "Human medical technology has little effect on your demonic body...but it's better than nothing.";
	}
}

class HigherHealthBonus : HealthBonus replaces HealthBonus
{
	Default
	{
		Inventory.MaxAmount 9999;
		Scale 0.7;
		Inventory.PickupMessage "A tonic made from a weak soul. Ingesting it fortifies your body.";
		Inventory.PickupSound "misc/HealthBonus";
		+Bright
	}
}

class BiggerHealthBonus : ResourceItemBase
{
	Default
	{
		Inventory.PickupMessage "You've consumed a demon's heart. Its essence fortifies your body.";
		Inventory.PickupSound "misc/HeartBonus";
		Scale 0.5;
		+INVENTORY.ALWAYSPICKUP
	}

	States
	{
		Spawn:
			BHBS AAAAA 1 A_SetScale(scale.x + 0.01);
			BHBS BBBBB 1 A_SetScale(scale.x + 0.01);
			BHBS C 10 Bright A_PlaySound("items/HeartBeat1");
			BHBS BBBBB 1 A_SetScale(scale.x - 0.01);
			BHBS AAAAA 1 A_SetScale(scale.x - 0.01);
			BHBS A 10 A_PlaySound("items/HeartBeat2");
			Loop;
		Pickup:
			TNT1 A 0 A_RestoreResource(0, 50, 0);
			Stop;
	}
}

class BiggestHealthBonus : ResourceItemBase
{
	Default
	{
		Inventory.PickupMessage "You've consumed a Baron's heart. Its essence greatly fortifies your body.";
		Scale 0.75;
		Inventory.PickupSound "misc/HeartBonus";
	}

	States
	{
		Spawn:
			BHRT AAA 1 A_SetScale(scale.x + 0.02);
			BHRT BBB 1 A_SetScale(scale.x + 0.02);
			BHRT C 6 Bright A_PlaySound("items/HeartBeat1");
			BHRT BBB 1 A_SetScale(scale.x - 0.02);
			BHRT AAA 1 A_SetScale(scale.x - 0.02);
			BHRT A 6 A_PlaySound("items/HeartBeat2");
			Loop;
		Pickup:
			TNT1 A 0 A_RestoreResource(0, 100, 0);
			Stop;
	}
}

class BetterSoulSphere : ResourceItemBase replaces Soulsphere
{
	Default
	{
		Inventory.PickupMessage "An orb containing a legion of souls. They fortify your body and restore your arcane power.";
		Inventory.PickupSound "misc/SpherePickup";
		Scale 0.2;
		FloatBobPhase 1;
		+DONTGIB
		+FLOATBOB

	}

	States
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIf(invoker.bDropped && random(0,1) == 1, "DropNothing");
			SOUL E 3 Bright A_SpawnItemEx("SoulSphereFlame", frandom(-6, 6), frandom(-6, 6), frandom(14, 18), 0, 0, 2);
			Loop;

		DropNothing:
			Stop;

		Pickup:
			TNT1 A 0
			{
				A_RestoreResource(0, 100, 50);
				A_GiveSoulgem(1);
			}
			Stop;
	}
}

//------------------------------------------------------
//
// MANA ITEMS
//
//------------------------------------------------------

class ManaSoul : ResourceItemBase
{
	Default
	{
		Inventory.PickupMessage "You've consumed the soul. It restores your arcane energy and heals your wounds.";
		Inventory.PickupSound "misc/SoulPickup";
		Renderstyle "Add";
		Scale 0.25;
		+DONTGIB
	}

	States
	{
		Spawn:
			BRMA ABCDEFGHIJLMN 4 Bright;
			Loop;
		Pickup:
			TNT1 A 0 A_RestoreResource(10, 0, 10);
			Stop;
	}
}

class BigBaronMana : ResourceItemBase
{
	Default
	{
		+INVENTORY.ALWAYSPICKUP
		Inventory.PickupMessage "You've consumed a larger soul. It greatly restores your arcane energy and fortifies your body.";
		Inventory.PickupSound "misc/SoulPickup";
		Renderstyle "Add";
		Alpha 0.75;
		Scale 0.5;
		+DONTGIB
	}

	states
	{
		Spawn:
			BRLM ABCDEFGHIJKLMNOPQRSTU 2 Bright;
			Loop;
		Pickup:
			TNT1 A 0 A_RestoreResource(15, 10, 25);
			Stop;
	}
}

//-------------------------------------------------------------
//
// SOULGEMS & MANA INCREASERS
//
//-------------------------------------------------------------

class SoulGem : ResourceItemBase
{
	Default
	{
		+COUNTITEM
		+FLOATBOB
		+DONTGIB
		+Bright
		+INVENTORY.ALWAYSPICKUP
		FloatBobStrength 0.5;
		FloatBobPhase 3;
		Inventory.PickupMessage "A Soul Gem. It draws in souls, and you will draw on it's souls when your mana runs out. Mana capacity increased.";
		Inventory.PickupSound "SILENTI1";
		Scale 0.20;
	}

	States
	{
		Spawn:
			TNT1 A 0 NoDelay
			{
				if (!hod_soulgem_spawn)
				{
					return ResolveState("DontSpawn");
				}
				Return ResolveState(null);
			}
			TNT1 A 0 A_JumpIf(invoker.bDropped && random(0, 1) == 1, "DontSpawn");
		SitThere:
			SGEM ABCDEFGHIJ 3 { if(!random(0, 5)) { A_SpawnItemEx("BaseSparkle", frandom(-10, 10), frandom(-10, 10), frandom(20, 35)); } }
			Loop;

		DontSpawn:
			TNT1 A 0;
			Stop;

		Pickup:
			TNT1 A 0
			{
				A_GiveSoulgem(1, 1);
				A_PlaySound("misc/SoulGemPickup", CHAN_7);
			}
			Stop;
	}
}

class SoulGemFragment : CustomInventory
{

	override void tick()
	{
		if(!random(0, 50))
		{
			A_SpawnItemEx("BaseSparkle", random(-10, 10), random(-10, 10), 20);
		}

		if(!random(0, 15))
		{
			A_SpawnItemEx("BaronSparks", frandom(-10, 10), frandom(-10, 10), 10);
		}

		Super.Tick();
	}
	Default
	{
		Inventory.PickUpSound "items/Fragments";
		Inventory.PickUpMessage "A broken fragment of a Soul Gem. With enough pieces, it could be repaired...";
		Scale 0.2;
		FloatBobStrength 0.5;
		FloatBobPhase 3;
		+DONTGIB;
		+COUNTITEM;
		+ROLLSPRITE;
		+ROLLCENTER;
		+FLOATBOB;
		+Bright;
	}

	States
	{
		Spawn:
			FGEM A -1 NoDelay
			{
				A_SetRoll(frandom(0, 360));
				A_SetScale(frandom(0.05, 0.15));
				if (random(1, 2) == 2) {invoker.bSpriteFlip = TRUE;}

				if (!hod_fragment_spawn || invoker.bDropped && !random(0, 1))
				{
					return ResolveState("DontSpawn");
				}
				Return ResolveState(null);
			}
			Stop;

		DontSpawn:
			TNT1 A 0 A_SpawnItemEx("BigBaronMana");
			Stop;

		DropNothing:
			TNT1 A 0;
			Stop;

		PickUp:
			TNT1 A 0
			{
				A_GiveInventory("FragmentCounter", 1);
				if (CheckInventory("FragmentCounter", 5))
				{
					A_GiveInventory("SoulGem", 1);
					A_PlaySound("FRAGFUSE", CHAN_AUTO);
					A_TakeInventory("FragmentCounter", 0);
				}
			}
			Stop;
	}
}


class FragmentCounter : Inventory
{
	Default
	{
		Inventory.MaxAmount 5;
	}
}

class NewMegasphere: ResourceItemBase
{
	Default
	{
		+COUNTITEM
		+FLOATBOB
		+INVENTORY.ALWAYSPICKUP
		FloatBobPhase 2;
		Inventory.PickupMessage "The souls of sinners greatly fortifies your body and restores your arcane power.";
		Inventory.PickupSound "misc/SpherePickup";
	}

	States
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIf(invoker.bDropped && random(0,1) == 1, "DropNothing");
			MEGA ABCDCBA 3 Bright
			{
				int width;
				width = 12;

				if (random(1, 4) == 1) { A_SpawnItemEx("SoulSphereFlame", frandom(-width, width), frandom(-width, width), frandom(14, 18), 0, 0, 2); }
				if (random(1, 4) == 2) { A_SpawnItemEx("MegaSphereFlame", frandom(-width, width), frandom(-width, width), frandom(14, 18), 0, 0, 2); }
				if (random(1, 4) == 3) { A_SpawnItemEx("MegaSphereFlame2", frandom(-width, width), frandom(-width, width), frandom(14, 18), 0, 0, 2); }
				if (random(1, 4) == 4) { A_SpawnItemEx("MegaSphereFlame3", frandom(-width, width), frandom(-width, width), frandom(14, 18), 0, 0, 2); }
			}
			Loop;

		DropNothing:
			Stop;

		Pickup:
			TNT1 A 0 A_GiveSoulgem(3);
			TNT1 A 0 A_RestoreResource(0, 500, 200);
			Stop;
	}
}

class BruiserSphere : ResourceItemBase
{
	Default
	{
		Inventory.Pickupmessage "The fires of Hell enter your body. You can become a Bruiser Demon!";
		Inventory.PickupSound "misc/SpherePickup";
		+FLOATBOB
	}

	States
	{
		Spawn:
			SPHR A 1 NoDelay Bright 
			{
				A_SpawnItemEx("BruiserFireTrail",frandom(-8,8),frandom(-8,8),frandom(10,29),0,0,frandom(1,4));
			}
			Loop;

		Pickup:
			TNT1 A 0
			{
				A_GiveSoulgem(5);
				A_GiveInventory("NewInvul", 1);
				let plr = TheBaron(self);
				if (plr)
				{
					plr.RageMeter += 1000;
				}
				A_RestoreResource(0, 500, 200);
			}
			Stop;
	}
}

//------------------------------------------------------
//
// GENERIC REPLACERS
//
//------------------------------------------------------
class HealthOverArmor : HigherHealthBonus replaces ArmorBonus {}
class HealthOverArmor2 : BiggestHealthBonus replaces BlueArmor {}
class HealthOverArmor3 : BiggerHealthBonus replaces GreenArmor {}
class BaronMana2: ManaSoul replaces RocketAmmo {}
class BaronMana3: ManaSoul replaces Cell {}
