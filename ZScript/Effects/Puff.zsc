class NullPuff : Actor
{
	States
	{
		Spawn:
			Stop;
		Death:
			Stop;
		Crash:
			Stop;
		XDeath:
			Stop;
	}
}

class PointBlankPuff : NullPuff
{
	Default
	{
		+ExtremeDeath;
		ProjectileKickBack 100;
	}
}

class FlakSparks : Actor
{
	Default
	{
		Projectile;
		Scale 0.15;
		RenderStyle "Add";
		Decal "BulletChip";
		+ThruGhost
		+ROllSprite
	}

	States
	{
		Spawn:
			SPKO C 1 Bright NoDelay
			{
				A_PlaySound("BulletImpact",0,1,false,ATTN_STATIC);
				A_SetRoll(random(0,360));
				if (hod_smoke)
				{
					A_SpawnProjectile("DebrisSmoke",0,0,frandom(-45,45),CMF_AIMDIRECTION, frandom(-25,25));
				}
				if (hod_embers)
				{
					for (int i = 0; i < 2; ++i)
						A_SpawnProjectile("Sparks",0,frandom(-4,4),frandom(-45,45),CMF_AIMDIRECTION, frandom(-35,35));
				}
			}
			SPKO CC 1 Bright A_SetScale(Scale.X + 0.05);
			Stop;
	}
}

class FlakShardImpact : FlakSparks
{
	Default
	{
		Scale 0.15;
		Alpha 0.75;
		Decal "BulletChip";
		+ROLLSPRITE
	}

	States
	{
		Spawn:
			SPKR A 1 Bright NODELAY
			{
				A_PlaySound("BulletImpact", CHAN_AUTO);
				A_SetRoll(random(0,360));
				if (hod_smoke)
				{
					A_SpawnProjectile("DebrisSmoke",0,0,frandom(155,205),CMF_AIMDIRECTION, frandom(-45,45));
				}
				if (hod_embers)
				{
					for (int i = 0; i < 2; ++i)
						A_SpawnProjectile("BelialSparks",0,frandom(-4,4),frandom(145,215),CMF_AIMDIRECTION, frandom(-35,35));
				}
				if (hod_rubble)
				{
					for (int i = 0; i < random(0, 1); ++i)
						A_SpawnProjectile("RubbleSpawner",0,frandom(-4,4),frandom(145,215),CMF_AIMDIRECTION, frandom(-35,35));
				}
			}
			SPKR AAAA 1 Bright A_SetScale(Scale.X +0.05);
			Stop;
	}
}

class MagicBelialPUff : Actor
{
	Default
	{
		+NOINTERACTION;
		+ROLLSPRITE;
		+ALWAYSPUFF;
		+PUFFONACTORS;
		+WALLSPRITE;
		+NoDamageThrust;
		+CAUSEPAIN;
		+FORCEPAIN;
		+BRIGHT;
		DamageType "Magic";
		Renderstyle "ADD";
		Decal "BulletChip";
		Scale 0.15;
		Alpha 0.75;
	}

	States
	{
		Spawn:
			FLEX A random(10, 45) NODELAY
			{
				A_SetRoll(random(0,360));
				if (hod_embers)
				{
					for (int i = 0; i < random(0, 4); ++i)
						A_SpawnProjectile("BaronEmber", 1, 0, frandom(0, 360), CMF_AIMDIRECTION, FRANDOM(-90, 90));
					for (int i = 0; i < random(4, 12); ++i)
						A_SpawnProjectile("BaronSparks", 1, 0, frandom(0, 360), CMF_AIMDIRECTION, FRANDOM(-90, 90));
				}
			}
			FLEX AA 1 A_SetScale(Scale.X + 1.25);
			FLEX A 1 A_SpawnItemEx("FireballExplosion0");
			Stop;

	}
}

class Type99Puff : FlakSparks
{
	Default
	{
		Scale 0.05;
		Decal "HODBulletHole";
		DamageType "Piercing";
		+ROLLSPRITE;
		+PUFFONACTORS
		+BLOODSPLATTER
	}

	States
	{
		Spawn:
			TNT1 A 0;
			Stop;
		Crash:
			SPKO B 1 Bright
			{
				A_PlaySound("BulletImpact", CHAN_AUTO);
				A_SetRoll(frandom(0,360));
				A_SprayDecal("ScorchMark2");
				if (hod_smoke)
				{
					for (int i = 0; i < 2; ++i)
						A_SpawnProjectile("DebrisSmoke",0,0,frandom(-35, 35),CMF_AIMDIRECTION, frandom(-45,45));
					for (int i = 0; i < 1; ++i)
						A_SpawnItemEx("FireballSmoke",frandom(-5,5),frandom(-5,5),frandom(-5,5),0,frandom(-3, 3),frandom(0,2));
				}
				if (hod_embers)
				{
					for (int i = 0; i < 4; ++i)
						A_SpawnProjectile("Sparks",0,frandom(-4,4),frandom(-35, 35),CMF_AIMDIRECTION, frandom(-35,35));
				}
				if (hod_rubble)
				{
					for (int i = 0; i < 4; ++i)
						A_SpawnProjectile("RubbleSpawner",0,frandom(-4,4),frandom(-35,35),CMF_AIMDIRECTION, frandom(-55,55));
				}
				if (hod_flames)
				{
					for (int i = 0; i < random(1, 3); ++i)
						A_SpawnItemEx("Type99Flame", 0, frandom(-16, 16), frandom(-24, 0), 0, frandom(-3, 3), frandom(-2, 2));
				}
			}
			SPKO CC 1 Bright { A_SetScale(Scale.X + 0.3); A_Fadeout(0.33); }
			Stop;
		Death:
		XDeath:
			TNT1 A 1
			{
				if (hod_flames)
				{
					for (int i = 0; i < random(1, 3); ++i)
						A_SpawnItemEx("Type99Flame", 0, frandom(-16, 16), frandom(-24, 0), 0, frandom(-3, 3), frandom(-2, 2));
				}
				if (hod_embers)
				{
					for (int i = 0; i < 4; ++i)
						A_SpawnProjectile("Sparks",0,frandom(-4,4),frandom(-35, 35),CMF_AIMDIRECTION, frandom(-35,35));
				}
			}
			Stop;
	}
}

class MagicType99Puff : Type99Puff
{
	Default
	{
		DamageType "MagicBurnFire";
	}

	States
	{
		Spawn:
			TNT1 A 0;
			Stop;
		Crash:
			SPKG C 1 Bright
			{
				A_PlaySound("BulletImpact", CHAN_AUTO);
				A_SetRoll(frandom(0,360));
				A_SprayDecal("ScorchMark2");
				if (hod_smoke)
				{
					for (int i = 0; i < 2; ++i)
						A_SpawnProjectile("DebrisSmoke",0,0,frandom(-35, 35),CMF_AIMDIRECTION, frandom(-45,45));
					for (int i = 0; i < 1; ++i)
						A_SpawnItemEx("FireballSmoke",frandom(-5,5),frandom(-5,5),frandom(-5,5),0,frandom(-3, 3),frandom(0,2));
				}
				if (hod_embers)
				{
					for (int i = 0; i < 4; ++i)
						A_SpawnProjectile("BaronSparks",0,frandom(-4,4),frandom(-35, 35),CMF_AIMDIRECTION, frandom(-35,35));
				}
				if (hod_rubble)
				{
					for (int i = 0; i < 4; ++i)
						A_SpawnProjectile("RubbleSpawner",0,frandom(-4,4),frandom(-35,35),CMF_AIMDIRECTION, frandom(-55,55));
				}
				if (hod_flames)
				{
					for (int i = 0; i < random(1, 3); ++i)
						A_SpawnItemEx("GreenFire", 0, 0, 0, 0, frandom(-2, 2), frandom(0, 3));
				}
			}
			SPKG CC 1 Bright A_SetScale(Scale.X + 0.4);
			Stop;
		Death:
		XDeath:
			TNT1 A 1;
			Stop;
	}
}

class AzazelBulletPuff: Type99Puff
{
	Default
	{
		Alpha 0.85;
		Decal "AzalChip";
		+PUFFONACTORS;
	}
	States
	{
		Spawn:
			TNT1 A 0 NoDelay A_SprayDecal("ScorchMark2");
		Crash:
			SPKO C 1
			{
				A_PlaySound("BulletImpact", CHAN_AUTO);
				A_SetRoll(frandom(0,360));

				if (hod_smoke)
				{
					for (int i = 0; i < 2; ++i)
						A_SpawnProjectile("DebrisSmoke",0,0,frandom(-35, 35),CMF_AIMDIRECTION, frandom(-45,45));
					for (int i = 0; i < 1; ++i)
						A_SpawnItemEx("FireballSmoke",frandom(-5,5),frandom(-5,5),frandom(-5,5),0,frandom(-3, 3),frandom(0,2));
				}
				if (hod_embers)
				{
					for (int i = 0; i < 4; ++i)
						A_SpawnProjectile("Sparks",0,frandom(-4,4),frandom(-35, 35),CMF_AIMDIRECTION, frandom(-35,35));
				}
				if (hod_rubble)
				{
					for (int i = 0; i < 4; ++i)
						A_SpawnProjectile("RubbleSpawner",0,frandom(-4,4),frandom(-35,35),CMF_AIMDIRECTION, frandom(-55,55));
				}
				if (hod_flames)
				{
					for (int i = 0; i < random(1, 3); ++i)
						A_SpawnItemEx("NapalmFireAfterImage", 0, frandom(-10, 10), frandom(-2, 6), 0, frandom(-4, 4), frandom(0, 4));
				}
			}
		Death:
		XDeath:
			SPKO C 1 Bright A_SpawnItemEx("AzazelBulletExplosion");
			SPKO CC 1 Bright A_SetScale(Scale.X + 0.4);
			Stop;
	}
}

class AzazelBulletExplosion : Actor
{
	Default
	{
		Alpha 0.65;
	//	Decal "ScorchMark2";
		Renderstyle "ADD";
		Scale 0.1;
		+NOGRAVITY;
		+PUFFONACTORS;
	}
	States
	{
		Spawn:
			TNT1 A 0 NoDelay
			{
				if (hod_embers)
				{
					for (int i = 0; i < 3; ++i)
						A_SpawnItemEx("BruiserEmber", 0, 0, 0, frandom(-6.0, 6.0), frandom(-6.0, 6.0), frandom(0.0, 6.0));
					for (int i = 0; i < 4; ++i)
						A_SpawnItemEx("Sparks", 0, 0, 0, frandom(-8.0, 8.0), frandom(-8.0, 8.0), frandom(0.0, 8.0));
				}
				A_PlaySound("AzazelExplode", CHAN_AUTO);
				if (hod_flames)
				{
					A_SpawnItemEx("AzazelFlare");
					A_SpawnITemEx("PunchDustRing");
					for (int i = 0; i < random(1, 5); ++i)
						A_SpawnItemEx("Type99Flame", 0, 0, 0, frandom(1, 3), frandom(-2, 2), frandom(0, 2));
				}
				if(hod_quake)
				{
					A_Quake(2, 35, 0, 128, "Null");
				}
			}
			NDEX A 1 Bright A_Explode(32, 64, XF_HURTSOURCE, TRUE, 0, 0, 0, "BulletPuff", "Fire");
			NDEX AABC 1 Bright A_SetScale(Scale.X + 0.15);
			TNT1 A 0
			{
				if (hod_smoke)
				{
				for (int i = 0; i < 3; ++i)
					A_SpawnItemEx("FireballSmoke",frandom(-5,5),frandom(-5,5),frandom(-5,5),0,frandom(-3, 3),frandom(0,2));
				}
			}
			NDEX DEFGHIJK 1 Bright A_SetScale(Scale.X + 0.04);
			NDEX NMO 1 Bright A_FadeOut(0.25);
			Stop;
	}
}

class MagicAzPuff : actor
{
	Default
	{
		Projectile;
		+ALWAYSPUFF
		+PUFFONACTORS
		+Bloodsplatter
		+NoGravity
		+Bright;
		+PUFFGETSOWNER

		DamageType "Magic";

		Scale 0.3;
		Alpha 0.25;
		RenderStyle "Add";
	}

	States
	{
		Spawn:
			TNT1 A 1 NoDelay;
			Stop;
		Crash:
			TNT1 A 0
			{
				if (hod_rubble)
				{
					for (int i = 0; i < 4; i += 1)
						A_SpawnItemEx("Rubble", random(-5, 5), random(-5, 5), 5, frandom(-25, 25), frandom(-25, 25), frandom(-2, 45));
				}
				if (hod_smoke)
				{
					for (int i = 0; i < 3; ++i)
						A_SpawnItemEx("DebrisSmoke",0,0,0, -3,frandom(-10,10),frandom(0,5));
				}
			}
		Death:
		XDeath:
			SPKG D 1 BRIGHT
			{
				if (hod_embers)
				{
					for (int i = 0; i < 4; ++i)
						A_SpawnProjectile("BaronEmber", 1, 0, frandom(0, 360), CMF_AIMDIRECTION, FRANDOM(-90, 90));
					for (int i = 0; i < 8; ++i)
						A_SpawnProjectile("BaronSparks", 1, 0, frandom(0, 360), CMF_AIMDIRECTION, FRANDOM(-90, 90));
				}
				if (hod_smoke)
				{
					for (int i = 0; i < 1; ++i)
						A_SpawnItemEx("FireballSmoke",frandom(-5,5),frandom(-5,5),frandom(-5,5),0,frandom(-3, 3),frandom(0,2));
				}
				if (hod_flames)
				{
					for (int i = 0; i < 3; ++i)
						A_SpawnItemEx("BBFlame", 0, 0, 0, -2, frandom(-4, 4), frandom(-3, 3));

						A_SpawnItemEx("BaronFlare",0,0,0,0,0,0,0,SXF_SETTARGET|SXF_ORIGINATOR);
				}
			}
			SPKG DDD 1 Bright A_SetScale(Scale.X + 0.3);
			Stop;
	}
}

class MagicAzPuff2 : MagicAzPuff
{
	STATES
	{
		Crash:
			TNT1 A 0
			{
				if (hod_rubble)
				{
					for (int i = 0; i < 12; i += 1)
						A_SpawnItemEx("Rubble", random(-5, 5), random(-5, 5), 5, frandom(-25, 25), frandom(-25, 25), frandom(-2, 45));
				}
				if (hod_smoke)
				{
					for (int i = 0; i < 8; ++i)
						A_SpawnItemEx("DebrisSmoke",0,0,0, -3,frandom(-10,10),frandom(0,5));
				}
			}
		Death:
		XDeath:
			SPKG D 1
			{
				A_Explode(256, 128, 0, TRUE, 64, 0, 0, "BulletPuff", "Magic");
				A_PlaySound("EXPLOD3", CHAN_AUTO);
				A_SpawnItemEx("ExplosionShockwave2");
				A_RadiusGIve("GreenExplosionFlash2", 1056, RGF_PLAYERS);
				for (int i = 0; i < 6; ++i)
					A_SpawnItemEx("GreenBoomWithSmoke", frandom(-45,45), 0, frandom(-45,45), 0, frandom(-3,3), frandom(0,3), frandom(0,360));
				if (hod_flames)
					{
						for (int i = 0; i < random(3, 8); ++i)
							A_SpawnITemEx("GreenBoomWithTrail",0,0,0,frandom(-15,15),frandom(-15,15),frandom(0,11));
						for (int i = -8; i <= 8; i += 1)
							A_SpawnItemEx("FlameAfterImage", frandom(-18.0, 18.0), frandom(-18.0, 18.0), i, 0, frandom(-19, 19), frandom(0, 22));
					}
				if (hod_embers)
				{
					for (int i = 0; i < 10; ++i)
						A_SpawnProjectile("BaronEmber", 0, 0, random(0, 360), CMF_AIMDIRECTION, random(-75, -15));
					for (int i = 0; i < 20; ++i)
						A_SpawnProjectile("BaronSparks", 0, 0, random(0, 360), CMF_AIMDIRECTION, random(-90, 0));
				}
				for (int i = 0; i < 3; ++i)
					A_SpawnItemEx("GEExplosion", frandom(-32, 32), frandom(-32, 32), frandom(-6, 32));
			}
			SPKG D 3
			{
				A_SetScale(Scale.X + 0.3);
				for (int i = 0; i < 6; ++i)
					A_SpawnItemEx("GEExplosion", frandom(-64, 64), frandom(-64, 64), frandom(28, 64));
				for (int i = 0; i < 6; ++i)
					A_SpawnItemEx("GreenBoomWithSmoke", frandom(-90,90), 0, frandom(45, 75), 0, frandom(-3,3), frandom(0,3), frandom(0,360));
			}
			SPKG D 3
			{
					A_SetScale(Scale.X + 0.3);
					for (int i = 0; i < 9; ++i)
						A_SpawnItemEx("GEExplosion", frandom(-32, 32), frandom(-32, 32), frandom(32, 96));
					for (int i = 0; i < 6; ++i)
						A_SpawnItemEx("GreenBoomWithSmoke", frandom(-45,45), 0, frandom(75, 95), 0, frandom(-3,3), frandom(0,3), frandom(0,360));
				}
			SPKG D 3
			{
					A_SetScale(Scale.X + 0.3);
					for (int i = 0; i < 12; ++i)
						A_SpawnItemEx("GEExplosion", frandom(-18, 18), frandom(-18, 18), frandom(64, 128));
					for (int i = 0; i < 6; ++i)
						A_SpawnItemEx("GreenBoomWithSmoke", frandom(-25, 25), 0, frandom(95, 130), 0, frandom(-3,3), frandom(0,3), frandom(0,360));
				}
			Stop;
	}
}

class BigBulletPuff : BulletPuff
{
	Default
	{
		Renderstyle "Add";
	}

	States
	{
		Spawn:
			GFAN CEGI 1 Bright;
			Stop;
	}
}
