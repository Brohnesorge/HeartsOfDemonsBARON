gameinfo
{
	TitleMusic = "sounds/title/BARONTITLE2.mid"
	PlayerClasses = "TheBaron"
	CreditPage = "CREDB0"
	StatusbarClass = "BaronHUD"
	AddEventHandlers = "HealthDropHandler", "MiscBaronHandler"
}
	
ClearSkills

Skill Coward
{
	AutoUseHealth
	AmmoFactor = 2
	HealthFactor = 2
	DamageFactor = 0.5
	EasyKey
	EasyBossBrain
	SpawnFilter = Baby
	Name = "Coward"
	MustConfirm = "Revenge will be too easy here. It will not quell your anger."
}

Skill Ogre
{
	SpawnFilter = Easy
	Name = "Ogre"
}

Skill KnightofHell
{
	SpawnFilter = Normal
	Name = "Knight of Hell"
}

Skill BaronOfHell
{
	SpawnFilter = Hard
	Name = "Baron of Hell"
}

Skill ArchonofHell
{
	SpawnFilter = Nightmare
	Name = "Archon of Hell"
	MustConfirm = "Revenge will be most difficult, but most satisfying. The Spiders' forces are much stronger and carry more ammunition. Souls you absorb heal you less. "
	AmmoFactor = 1.5
	DamageFactor = 2.0
	MonsterHealth = 1.5
    FastMonsters
}

Skill IconOfSin
{
	SpawnFilter = Nightmare
	Name = "Icon of Sin"
	MustConfirm = "Don't do it. Just don't. As Archon of Hell, but enemies never stop and are more aggressive."
	AmmoFactor = 1.00
	DamageFactor = 1.50
	MonsterHealth = 1.50
    FastMonsters
	NoPain
	Aggressiveness = 0.45
    DisableCheats
	RespawnTime = 12
}